# ![st logo](st.png "st logo")
st is a simple terminal emulator for X which sucks less.


## Requirements
In order to build st you need the Xlib header files.


## Installation
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):
```
make clean install
```

## Running st
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:
```
tic -sx st.info
```

See the man page for additional details.

## Credits
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

## Patches
This source was modified and contains following patches:
* st-font2-20190416-ba72400.diff
* st-scrollback-20190331-21367a0.diff
* st-scrollback-mouse-0.8.diff
* st-scrollback-mouse-altscreen-20190131-e23acb9.diff
* st-xresources-20190105-3be4cf1.diff

Keybindings:
* Alt-0 - Reset font size to default.
* Alt-= - Increase font size.
* Alt-- - Decrease font size.
* Alt-c - Copy the selected text to the clipboard selection.
* Alt-v - Paste from clipboard selection.
* Alt-p - Paste from primary selection (middle mouse button).
* Alt-j - Scroll down.
* Alt-k - Scroll up.
* Alt-u - Scroll down (page).
* Alt-i - Scroll up (page).